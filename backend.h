#ifndef BACKEND_H
#define BACKEND_H

#include <QObject>
#include <QQmlEngine>



class Backend : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString message READ getMessage)
public:
    explicit Backend(
        QObject *parent = nullptr
    );
    QString getMessage() const{
        return message;
    }

    Q_INVOKABLE
    void validate() const;

signals:

private:
    const QString message = "hello";
};

#endif // BACKEND_H
