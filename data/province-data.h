#ifndef DATA_PROVINCEDATA_H
#define DATA_PROVINCEDATA_H

#include <QString>
#include <QColor>
#include <QImage>
#include <QVector>
#include <QHash>

namespace data {

struct CellData{
    QPoint coordinates;
    bool border;
};

struct ProvinceData
{
    int id;
    QColor color;
    QVector<CellData> cells;
    QPoint center;
    QHash<int, int> distances;
};

} // namespace data

#endif // DATA_PROVINCEDATA_H
