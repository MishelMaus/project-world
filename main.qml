import QtQuick
import QtQuick.Window
import QtQuick.Controls 2.12

import backend

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Project world")

    FontLoader {
        id: cormorantGaramond
        source: "fonts/CormorantGaramond-Regular.ttf"
    }

    Button{
        anchors.centerIn: parent
        id: beginButton
        text: qsTr("Begin")

        contentItem: Text {
            text: beginButton.text
            font.family: cormorantGaramond.name
            font.pixelSize: 16
            opacity: enabled ? 1.0 : 0.3
            color: beginButton.down ? "#17a81a" : "#21be2b"
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            elide: Text.ElideRight
        }

        background: Rectangle {
            implicitWidth: 100
            implicitHeight: 40
            opacity: enabled ? 1 : 0.3
            border.color: beginButton.down ? "#17a81a" : "#21be2b"
            border.width: 1
            radius: 2
        }

        onClicked: {
            Backend.validate()
        }
    }
}
