#include "backend.h"
#include <QFile>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QColor>
#include <QImage>

#include "initialprovincedata.h"
#include "data/province-data.h"


int getProvinceIdByColor(const QVector<data::ProvinceData> provinces, const QColor color){
    for(int i = 0; i < provinces.size(); i++){
        if(provinces.at(i).color == color){
            return i;
        }
    }

    throw "Shame happened";
}

QString provinceMaskNameById(const int id){
    return QStringLiteral("output/province-%1-mask.png").arg(id);
}
QString provinceBorderMaskNameById(const int id){
    return QStringLiteral("output/province-%1-border-mask.png").arg(id);
}

QImage getProvinceMaskById(const int id){
    const auto fileName = provinceMaskNameById(id);
    return QImage(fileName);
}

QJsonDocument readJsonFromFile(const QString fileName)
{
    QFile file(fileName);

    if(!file.open(QIODevice::ReadOnly)){
        qWarning() << QStringLiteral("No %1 file provided").arg(fileName);
        return {};
    }

    return QJsonDocument::fromJson(
        file.readAll()
    );
}

QVector<InitialProvinceData> readInitialData(){
    const auto json = readJsonFromFile("data/initial-province-data.json");

    if (!json.isArray()){
        qWarning("No province names provided");
        return {};
    }

    QVector<InitialProvinceData> provinces;
    const auto asArray = json.array();

    for(int i = 0; i < asArray.size(); i++){
        const auto jsonEntry = asArray.at(i);
        if(!jsonEntry.isObject()){
            qWarning("Not an object");
            return {};
        }
        const QJsonObject jsonObject(
            jsonEntry.toObject()
        );

        const QJsonValue maybeColor(
            jsonObject["color"]
        );
        if(!maybeColor.isString()){
            qWarning("Not a string");
            return {};
        }
        const QString colorString(
            maybeColor.toString()
        );
        const QColor color(colorString);
        if(!color.isValid()){
            qWarning("Not a valid color");
            return {};
        }
        provinces.append({
            color
        });
    }

    return provinces;
}

QMultiHash<QRgb, QRgb> findAdjacentColors(){
    const QImage provinceMap("data/provinces.png");
    if(provinceMap.isNull()){
        qWarning("No province map provided");
        return QMultiHash<QRgb, QRgb>();
    }
    QMultiHash<QRgb, QRgb> adjacentColors;
    for(int i = 0; i < provinceMap.width(); i++){
        for(int j = 0; j < provinceMap.height(); j++){
            const QColor currentColor(provinceMap.pixelColor(i,j));

            const QColor rightColor(provinceMap.pixelColor((i + 1) % provinceMap.width(), j));
            if(currentColor != rightColor){
                if(adjacentColors.count(currentColor.rgba(), rightColor.rgba()) == 0){
                    adjacentColors.insert(currentColor.rgba(), rightColor.rgba());
                }
            }
            if(j != provinceMap.height() - 1){
                const QColor downColor(provinceMap.pixelColor(i, (j + 1) % provinceMap.height()));
                if(currentColor != downColor){
                    if(adjacentColors.count(currentColor.rgba(), downColor.rgba()) == 0){
                        adjacentColors.insert(currentColor.rgba(), downColor.rgba());
                    }
                }
            }
        }
    }

    return adjacentColors;
}

void calculateDistances(
        const QMultiHash<QRgb, QRgb> adjacentColors,
        QVector<data::ProvinceData>& provinces
){
    for(const auto key : adjacentColors.uniqueKeys()){
        const QColor sourceColor(key);
        const auto sourceId = getProvinceIdByColor(provinces, sourceColor);
        const auto sourceCenter = provinces.at(sourceId).center;
        for(const auto value : adjacentColors.values(key)){
            const QColor targetColor(value);
            const auto targetId = getProvinceIdByColor(provinces, targetColor);
            const auto targetCenter = provinces.at(targetId).center;

            const int distance = (targetCenter - sourceCenter).manhattanLength();
            provinces[sourceId].distances[targetId] = distance;
        }
    }
}

Backend::Backend(
        QObject *parent
) : QObject(parent)
{

}

void createMask(const QColor color, const int id)
{
    const QImage provinceMap("data/provinces.png");
    if(provinceMap.isNull()){
        qWarning("No province map provided");
        return;
    }


    const QImage mask(
        provinceMap.createMaskFromColor(
            color.rgb()
        )
    );
    const auto fileName = provinceMaskNameById(id);
    mask.save(fileName);
}

bool isBorder(const int x, const int y, const QImage mask){
    const QColor black("black");
    const auto width = mask.width();
    if(x == 0){
        if(mask.pixelColor(width-1, y) == black){
            return true;
        }
    } else if(x == width - 1){
        if(mask.pixelColor(0, y) == black){
            return true;
        }
    } else{
        if(mask.valid(x + 1, y)){
            if(mask.pixelColor(x + 1, y) == black){
                return true;
            }
        }
        if(mask.valid(x, y - 1)){
            if(mask.pixelColor(x, y - 1) == black){
                return true;
            }
        }
        if(mask.valid(x - 1, y)){
            if(mask.pixelColor(x - 1, y) == black){
                return true;
            }
        }
        if(mask.valid(x, y + 1)){
            if(mask.pixelColor(x, y + 1) == black){
                return true;
            }
        }
    }
    return false;
}

QVector<data::CellData> findCells(const int id){
    QColor white("white");
    const auto mask = getProvinceMaskById(id);
    QVector<data::CellData> ret;
    QImage borderMask(mask);
    for(int x = 0; x < mask.width(); x++){
        for(int y = 0; y < mask.height(); y++){
            const QColor color(mask.pixelColor(x, y));
            if(color == white){
                const auto border = isBorder(x, y, mask);
                ret.append({
                    .coordinates = {x, y},
                    .border = border
                });
                if(!border){
                    borderMask.setPixel(x, y, 0);
                }
            }
        }
    }
    borderMask.save(provinceBorderMaskNameById(id));
    return ret;
}

QPoint findCenter(const QVector<data::CellData> cells){
    int sumX = 0;
    int sumY = 0;
    int count = cells.size();

    for(int j = 0; j < cells.size(); j++){
        sumX += cells.at(j).coordinates.x();
        sumY += cells.at(j).coordinates.y();
        count++;
    }

    if(count != 0){
        sumX /= count;
        sumY /= count;
    }

    return {sumX, sumY};
}

void Backend::validate() const{
    const auto initialProvinces = readInitialData();

    QVector<data::ProvinceData> provinces;
    for(int i = 0; i < initialProvinces.size(); i++){
        const auto color = initialProvinces.at(i).color;
        createMask(color, i);
        auto cells = findCells(i);
        const auto center = findCenter(cells);
        provinces.append({
            .id = i,
            .color = color,
            .cells = cells,
            .center = center,
            .distances = {}
        });
    }
    const auto adjacentColors = findAdjacentColors();
    calculateDistances(adjacentColors, provinces);
}
